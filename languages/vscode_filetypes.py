# This dictionary maps an ultisnips file to a vscode language 
#
# Keys: ultisnips language prefix
# Values: vscode language mode
#
# Only languages that I use are currently here. Of course, they will need to be
# updated...

languages = {
    "markdown": "markdown",
    "python": "python",
    "json": "json",
    "sh": "shellscript",
    "bash": "shellscript",
    "zsh": "shellscript",
    "tex": "tex",
    "yaml": "yaml"
}
